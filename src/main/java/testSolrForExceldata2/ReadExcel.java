package testSolrForExceldata2;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReadExcel {

    String keyWordExcel;
    String keyWordSheet;

    public ReadExcel(String keyWordExcel,String keyWordSheet) {
        this.keyWordExcel = keyWordExcel;
        this.keyWordSheet = keyWordSheet;
    }

    public String getKeyWordExcel() {
        return keyWordExcel;
    }

    public String getKeyWordSheet() {
        return keyWordSheet;
    }

    /** 总行数 */
    private int totalRows = 0;
    /*public static void main(String[] args) throws Exception {
        String thirdName1= "";
        List list = readExcel(thirdName1,1,2);
        for(int i = 0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }*/
    /*
    * 如果thirdName == ”“，list为全部值
    * 如果thirdName == ”一个不存在的值“，list为空
    * 如果thirdName == ”存在的值“，list为符合的值
    * */
    public List readExcel(String thirdName,int thirdNameCell,int keyWordCell) throws Exception {
        File xlsFile = new File(getKeyWordExcel());
        Map map = new HashMap();
        List<String> list = new ArrayList<String>();
        boolean flag = false;
        // 工作表
        Workbook workbook = WorkbookFactory.create(xlsFile);
        /** 得到第一个shell */
        Sheet sheet = workbook.getSheet(getKeyWordSheet());
        /** 得到Excel的行数 */
        totalRows = sheet.getPhysicalNumberOfRows();
        /*
        * 如果传入了thirdName，那么把thirdName对应的关键词取出来，放到list里
        * 如果没有传入thirdName，那么把所有的关键词取出来，放到list里
        * */
        if(!("".equals(thirdName))){
            flag = true;
        }
        //1.循环totalRows行数，获取 thirdNameCell 列的数据
        for(int i = 0; i < totalRows; i++){
            Row row = sheet.getRow(i);//i行
            Cell rowForTNC = row.getCell(thirdNameCell);
            String rowForTN = "";
            if(rowForTNC.getCellType() == Cell.CELL_TYPE_STRING){
                rowForTN = rowForTNC.getStringCellValue();
            }else if(rowForTNC.getCellType() == Cell.CELL_TYPE_NUMERIC){
                rowForTN = String.valueOf(rowForTNC.getNumericCellValue());
            }
            if(null == rowForTN || "".equals(rowForTN)){
                continue;
            }
            if(flag){//如果thirdName不为空
                if(rowForTN.equals(thirdName)){//表格里有值，且等于需要的值
                    //取keyWordCell 里的值
                    Cell rowForKWC = row.getCell(keyWordCell);
                    //放到list里
                    list.add(rowForKWC.getStringCellValue());
                    continue;
                }
            }else{
                //取所有 keyWordCell 里的值,
                Cell rowForKWC = row.getCell(keyWordCell);
                list.add(rowForKWC.getStringCellValue());
            }
        }
        return list;
    }
}
