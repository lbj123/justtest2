package testSolrForExceldata2;

/**
 * @Author 李蓓杰
 * @function
 */
public class Path {
    static final String keyWordExcel="E:\\workspaceNoOne\\workspace\\运营\\运营·专业分析策略V1.1_1.xlsx";
    static final String keyWordSheet = "补充非泛职类关键词";

    static final String conn = "&";

    static final String solrForJD="https://solr:SolrRocks@zpsearch.zhaopin.com/solr-position-pre/positionCloud/select";//JDsolr地址
    static final String quRequest = "http://s-understand-server.zpidc.com/understandService/queryUnderstand";//qu地址 ?jobNumber=CC315880380J40078750308
    static final String captionRequest = "http://b-caption-jobcaption.zpidc.com/jobcaption/captionService/get";//qu地址 ?jobNumber=CC315880380J40078750308


    static final String thirdCode ="20000100010000";//三级职类code
    static final String thirdName1 = "";//三级职类名称
    static final String posType = "4";//工作性质
    static final String rows ="200";//查询条数
    static final String starts = "200"; //从第几条开始

    static final int thirdNameCell = 1;//要查询的sheet页的 三级职类名称 所在的 列
    static final int keyWordCell = 1;//要查询的sheet页的 关键词 所在的 列

    //static final String paramO = "fl=SOU_POSITION_ID,SOU_POSITION_NAME,JD_CHUNK_TITLE_BUSINESS_DIRCTION&fq=JD_ORG_RELATED_INDUSTRY_LEVEL1%3A*&fq=-JD_SKILL_TERMS_WEIGHT%3A*&fq=-JD_STD_SKILL%3A*&fq=-JD_UGC_SKILL%3A*&q=JD_JOB_LEVEL:";//参数
    static final String paramO = "fl=SOU_POSITION_ID,SOU_POSITION_NAME,JD_CHUNK_TITLE_BUSINESS_DIRCTION&fq=JD_STD_SKILL%3A*&fq=JD_UGC_SKILL%3A*&q=JD_JOB_LEVEL:";//参数
    //static final String paramT = "%20AND%20JD_NLP_JOBTYPE_REQUIRE:[*%20TO%20*]%20AND%20SOU_POSITION_TYPE:";//参数   JD_NLP_JOBTYPE_REQUIRE  JDNLP预测三级职类
    //static final String paramT = "%20AND%20SOU_SALARY_MIN:1000%20AND%20SOU_POSITION_TYPE:";//参数
    static final String paramT = "%20AND%20SOU_POSITION_TYPE:";//参数
    static final String paramTh = "%20AND%20SOU_POSITION_NAME:[*%20TO%20*]&start=0&rows=";//参数

    //一条查询参数
    static final String paramAll ="fl=SOU_POSITION_ID&fq=SOU_POSITION_TYPE:2&fq=JD_CHUNK_TITLE_KEY_FUNCTION:机电&q=JD_JOB_LEVEL:15000300220000&start=0&rows=200";


}
