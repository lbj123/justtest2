package testSolrForExceldata2;

import com.alibaba.fastjson.JSONObject;

/**
 * @Author
 * @function   获取 【职位名称】有多个的情况，比如：JD_TITLE_NORM_REDIS "配送司机;药品"
 */
public class TestRun2 {
    public static void main(String[] args) {
        //发送solr请求，获取json
        //获取json中的souid
        //发送qu请求，获取JD_TITLE_NORM_REDIS，判断是否有;号，有的话，为想要的数据
        /*String paramO = Path.paramO;
        String paramT = Path.paramT;
        String paramTh = Path.paramTh;

        String thirdCode = Path.thirdCode;
        String posType = Path.posType;
        String rows = Path.rows;
        String param =  paramO+thirdCode+paramT+posType+paramTh+rows;*/

        JSONObject responseJson = SolrRequestForL.doGet(Path.solrForJD,Path.paramAll);
        FilterResult.reduceResult2(responseJson);

    }
}
