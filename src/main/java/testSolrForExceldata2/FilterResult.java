package testSolrForExceldata2;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
//import org.apache.commons.lang3.StringUtils;
import java.util.*;

/**
 * @Author 李蓓杰
 * @function 对solr查回的结果进行筛选
 */
public class FilterResult {

    public static void reduceResult(JSONObject responseJson,List<String> list){ // responseJson 即solr查回的结果
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容
        List inExcel = new ArrayList();
        List norInExcel = new ArrayList();

        List need = new ArrayList();
        List needNotExcell = new ArrayList();

        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701

            //souName 是solr取出来的，和开发使用的qu库取出来的，【可能】有出入
            String quParam = "jobNumber="+souId;
            //发送qu请求
            JSONObject qureponse = SolrRequestForL.doGet(Path.quRequest,quParam);
            //获取 qu中的职位名称 JD_TITLE_NORM_REDIS
            JSONObject data = qureponse.getJSONObject("data");
            JSONObject job = data.getJSONObject("job");
            if(null == job){
                continue;
            }

            //JD_BEHAVIOR_FEATURE  -- JD_NLP_JOBTYPE_REQUIRE
            JSONObject JD_BEHAVIOR_FEATURE = job.getJSONObject("JD_BEHAVIOR_FEATURE");
            String JD_NLP_JOBTYPE_REQUIRE = JD_BEHAVIOR_FEATURE.getString("JD_NLP_JOBTYPE_REQUIRE");//"5000100020000|0.06666908011524618 5000100090000|0.0357155786331676"

            if(!("".equals(JD_NLP_JOBTYPE_REQUIRE) ||null == JD_NLP_JOBTYPE_REQUIRE )){
                String[] nlpTypeCol = JD_NLP_JOBTYPE_REQUIRE.split(" ");
                if(nlpTypeCol.length > 1){
                    need.add(souId);
                }
            }
            String jdTitleNorm = job.getString("JD_TITLE_NORM_REDIS");//有可能长这个样子：维保技工;消防维保，此时只要有一个在excel里，也算在excel里

            if("".equals(jdTitleNorm) || null == jdTitleNorm){
                continue;
            }
            String[] jdTitleNormCol = jdTitleNorm.split(";");

            for(int j=0;j<list.size();j++){
                //用 jdTitleNorm
                /*
                if(jdTitleNorm.equals(list.get(j))){
                    inExcel.add(souId);
                }*/

                //循环 jdTitleNormCol
                for(int k = 0;k<jdTitleNormCol.length;k++){
                    /*if(jdTitleNormCol[k].equals(list.get(j))){
                        inExcel.add(souId);
                        break;
                    }*/
                    if(jdTitleNormCol[k].indexOf(list.get(j)) != -1){
                        /*if(jdTitleNormCol.length > 1){
                            inExcel.add(souId+":"+jdTitleNormCol[k]);
                        }*/

                        inExcel.add(souId);
                        break;
                    }
                }

            }
        }
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            if(!(inExcel.contains(souId))){
                norInExcel.add(souId);
            }
        }
        //need.add(souId);
        for(int i = 0;i<norInExcel.size();i++){
            if((need.contains(norInExcel.get(i)))){
                needNotExcell.add(norInExcel.get(i));
            }
        }

        System.out.println("=====================以下数据在excel里面======================");

        List<String> inExcelNew = new ArrayList<String>(new TreeSet<String>(inExcel));

        for(Object aa:inExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(inExcelNew.size());
        System.out.println("=====================以下数据不在excel里面======================");
        List<String> norInExcelNew = new ArrayList<String>(new TreeSet<String>(norInExcel));
        for(Object aa:norInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(norInExcelNew.size());

        System.out.println("=====================以下数据不在excel里面 并且JD_NLP_JOBTYPE_REQUIRE为多个值======================");
        List<String> nlpnorInExcelNew = new ArrayList<String>(new TreeSet<String>(needNotExcell));
        for(Object aa:nlpnorInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(nlpnorInExcelNew.size());

    }

    public static void reduceResultforCap(JSONObject responseJson,List<String> list){ // responseJson 即solr查回的结果
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容
        List inExcel = new ArrayList();
        List norInExcel = new ArrayList();

        List need = new ArrayList();
        List needNotExcell = new ArrayList();

        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701

            //souName 是solr取出来的，和开发使用的caption库取出来的，【可能】有出入
            //发送caption请求
            String capParam = "type=jobForQuAggregatedCaption&id="+souId;
            JSONObject qureponse = SolrRequestForL.doGet(Path.captionRequest,capParam);
            JSONObject data = qureponse.getJSONObject("data");
            JSONObject job = data.getJSONObject("value");
            if(null == job){
                continue;
            }
            //JD_BEHAVIOR_FEATURE  -- JD_NLP_JOBTYPE_REQUIRE
            JSONObject JD_BEHAVIOR_FEATURE = job.getJSONObject("behaviorFeature");
            String JD_NLP_JOBTYPE_REQUIRE = JD_BEHAVIOR_FEATURE.getString("JD_NLP_JOBTYPE_REQUIRE");//"5000100020000|0.06666908011524618 5000100090000|0.0357155786331676"

            if(!("".equals(JD_NLP_JOBTYPE_REQUIRE) ||null == JD_NLP_JOBTYPE_REQUIRE )){
            //if(!(StringUtils.isBlank(JD_NLP_JOBTYPE_REQUIRE))){

                String[] nlpTypeCol = JD_NLP_JOBTYPE_REQUIRE.split(" ");
                if(nlpTypeCol.length > 1){
                    need.add(souId);
                }
            }
            //jobTitleTermWeight.results.[*].normResult
            JSONObject jobTitleTermWeight = job.getJSONObject("jobTitleTermWeight");
            JSONArray results = jobTitleTermWeight.getJSONArray("results");

            if(null == results || results.size() <= 0){  // || results为空，不会导致results.size() 报空指针
                continue;
            }

            for(int j=0;j<list.size();j++){
                //循环 results
                for(int k = 0;k<results.size();k++){
                    String normResult = ((JSONObject)results.get(k)).getString("normResult");
                    if(normResult.indexOf(list.get(j)) != -1){
                        inExcel.add(souId);
                        break;
                    }
                }

            }
        }
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            if(!(inExcel.contains(souId))){
                norInExcel.add(souId);
            }
        }
        //need.add(souId);
        for(int i = 0;i<norInExcel.size();i++){
            if((need.contains(norInExcel.get(i)))){
                needNotExcell.add(norInExcel.get(i));
            }
        }

        System.out.println("=====================以下数据在excel里面======================");

        List<String> inExcelNew = new ArrayList<String>(new TreeSet<String>(inExcel));

        for(Object aa:inExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(inExcelNew.size());
        System.out.println("=====================以下数据不在excel里面======================");
        List<String> norInExcelNew = new ArrayList<String>(new TreeSet<String>(norInExcel));
        for(Object aa:norInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(norInExcelNew.size());

        System.out.println("=====================以下数据不在excel里面 并且JD_NLP_JOBTYPE_REQUIRE为多个值======================");
        List<String> nlpnorInExcelNew = new ArrayList<String>(new TreeSet<String>(needNotExcell));
        for(Object aa:nlpnorInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(nlpnorInExcelNew.size());

    }
    public String aaa(){
        return "ddd";
    }

    //获取权重
    /*public static void reduceResultforCapForTest(JSONObject responseJson,List<String> list){ // responseJson 即solr查回的结果
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容
        List inExcel = new ArrayList();
        List norInExcel = new ArrayList();

        List need = new ArrayList();
        List needNotExcell = new ArrayList();

        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701

            //souName 是solr取出来的，和开发使用的caption库取出来的，【可能】有出入
            //发送caption请求
            String capParam = "type=jobForQuAggregatedCaption&id="+souId;
            JSONObject qureponse = SolrRequestForL.doGet(Path.captionRequest,capParam);

            //data.value.jobKnowledgeFeatures[*].leaf[0].kgId   weight
            JSONObject data = qureponse.getJSONObject("data");
            JSONObject job = data.getJSONObject("value");
            if(null == job){
                continue;
            }
            JSONArray jobKnowledgeFeatures = data.getJSONArray("jobKnowledgeFeatures");
            if(null==jobKnowledgeFeatures || "".equals(jobKnowledgeFeatures)){
                continue;
            }
            for (){

            }





            String JD_NLP_JOBTYPE_REQUIRE = JD_BEHAVIOR_FEATURE.getString("JD_NLP_JOBTYPE_REQUIRE");//"5000100020000|0.06666908011524618 5000100090000|0.0357155786331676"

            if(!("".equals(JD_NLP_JOBTYPE_REQUIRE) ||null == JD_NLP_JOBTYPE_REQUIRE )){
                //if(!(StringUtils.isBlank(JD_NLP_JOBTYPE_REQUIRE))){

                String[] nlpTypeCol = JD_NLP_JOBTYPE_REQUIRE.split(" ");
                if(nlpTypeCol.length > 1){
                    need.add(souId);
                }
            }
            //jobTitleTermWeight.results.[*].normResult
            JSONObject jobTitleTermWeight = job.getJSONObject("jobTitleTermWeight");
            JSONArray results = jobTitleTermWeight.getJSONArray("results");

            if(null == results || results.size() <= 0){  // || results为空，不会导致results.size() 报空指针
                continue;
            }

            for(int j=0;j<list.size();j++){
                //循环 results
                for(int k = 0;k<results.size();k++){
                    String normResult = ((JSONObject)results.get(k)).getString("normResult");
                    if(normResult.indexOf(list.get(j)) != -1){
                        inExcel.add(souId);
                        break;
                    }
                }

            }
        }
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            if(!(inExcel.contains(souId))){
                norInExcel.add(souId);
            }
        }
        //need.add(souId);
        for(int i = 0;i<norInExcel.size();i++){
            if((need.contains(norInExcel.get(i)))){
                needNotExcell.add(norInExcel.get(i));
            }
        }

        System.out.println("=====================以下数据在excel里面======================");

        List<String> inExcelNew = new ArrayList<String>(new TreeSet<String>(inExcel));

        for(Object aa:inExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(inExcelNew.size());
        System.out.println("=====================以下数据不在excel里面======================");
        List<String> norInExcelNew = new ArrayList<String>(new TreeSet<String>(norInExcel));
        for(Object aa:norInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(norInExcelNew.size());

        System.out.println("=====================以下数据不在excel里面 并且JD_NLP_JOBTYPE_REQUIRE为多个值======================");
        List<String> nlpnorInExcelNew = new ArrayList<String>(new TreeSet<String>(needNotExcell));
        for(Object aa:nlpnorInExcelNew){
            System.out.println(aa.toString());
        }
        System.out.println(nlpnorInExcelNew.size());

    }*/

    public static void reduceResult2(JSONObject responseJson){
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]
        List manyPos = new ArrayList();//需求1：存放多个职位名称
        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            System.out.println(souId);

        }

    }
}
