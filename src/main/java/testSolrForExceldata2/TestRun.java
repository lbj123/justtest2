package testSolrForExceldata2;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author 李蓓杰
 * @function
 */
public class TestRun {
    public static void main(String[] args) throws Exception {
        String aaa = System.getProperty("user.dir");
        System.out.println(aaa);
        System.out.println("123");

        ReadExcel readExcel  = new ReadExcel(Path.keyWordExcel,Path.keyWordSheet);
        List<String> list = new ArrayList<String>();

        String thirdName1= Path.thirdName1;
        list = readExcel.readExcel(thirdName1,Path.thirdNameCell,Path.keyWordCell);

        String paramO = Path.paramO;
        String paramT = Path.paramT;
        String paramTh = Path.paramTh;

        String thirdCode = Path.thirdCode;
        String posType = Path.posType;
        String rows = Path.rows;
        String param =  paramO+thirdCode+paramT+posType+paramTh+rows;

        JSONObject responseJson = SolrRequestForL.doGet(Path.solrForJD,param);
        //FilterResult.reduceResult(responseJson,list);
        FilterResult.reduceResultforCap(responseJson,list);

        /*String jdTitleNorm = "认识专员";
        String[] jdTitleNormCol = jdTitleNorm.split(";");
        System.out.println(jdTitleNormCol[0]);*/

    }
}
