package justsendsolrrequest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author 李蓓杰
 * @function 对solr查回的结果进行筛选
 */
public class FilterResult {

    public static void reduceResult(JSONObject responseJson){ // responseJson 即solr查回的结果
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容

        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            System.out.println(souId);
        }

    }


    //获取 JD_KNOWLEDGE_TEST_DIRCTION_TERM 的权重中有5 4 ，且权重5 4 都在excel中
    public static void reduceResult2(JSONObject responseJson){ // responseJson 即solr查回的结果
        JSONObject response = responseJson.getJSONObject("response");
        JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容

        int listSize = listArray.size();
        for(int i=0;i<listSize;i++){
            JSONObject sou = (JSONObject)listArray.get(i);
            //String souName = sou.getString("SOU_POSITION_NAME");//山东 济南####消防操作员
            String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
            System.out.println(souId);
        }

    }


}
