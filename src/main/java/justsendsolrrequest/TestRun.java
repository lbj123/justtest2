package justsendsolrrequest;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 李蓓杰
 * @function
 */
public class TestRun {
    public static void main(String[] args) throws Exception {


        //读txt中的三级职类，放在list中  begin
        /*String filePath = Path.filePath;
        List<String> fileList=new ArrayList<String>();//创建存储String的List
        try {
            fileList= Files.readAllLines(Paths.get(filePath));//读取该路径的文件
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        for (String aa:fileList){
            JSONObject responseJson = SolrRequestForL.doGet(Path.solrForJD,Path.paramO+aa+Path.paramT);
            FilterResult.reduceResult(responseJson);
        }*/
        //读txt中的三级职类，放在list中  end
        JSONObject responseJson = SolrRequestForL.doGet(Path.solrForJD,Path.paramAll);
        FilterResult.reduceResult(responseJson);
    }
}
