package testForvba;

import java.util.ArrayList;
import java.util.List;

public class TestVba01 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("20000100010000");
        list.add("20000100030000");
        list.add("20000100050000");
        list.add("20000100010000");
        list.add("20000100020000");
        list.add("20000100120000");
        list.add("20000100130000");
        list.add("20000100020000");
        list.add("20000100070000");
        list.add("20000100070000");
        list.add("20000100070000");
        list.add("20000100080000");
        list.add("20000100150000");
        list.add("20000100080000");
        List<String> listNew = new ArrayList<String>();
        for (String str : list) {
            if (!listNew.contains(str)) {
                listNew.add(str);
            }
        }
        for(int i = 0;i<listNew.size();i++){
            System.out.println(listNew.get(i));
        }
    }
}
