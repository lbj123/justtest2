package testSolrExceldata;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.*;


public class ReadExcel {
    /** 总行数 */
    private static int totalRows = 0;
    /** 总列数 */
    private static int totalCells = 0;
    /** 错误信息 */
    private static String errorInfo;

    public static void main(String[] args) throws Exception {
        File xlsFile = new File("E:\\workspaceNoOne\\workspace\\金融保险\\test.xlsx");
        Map map = new HashMap();
        // 工作表
        Workbook workbook = WorkbookFactory.create(xlsFile);


        /** 得到第一个shell */
        Sheet sheet = workbook.getSheetAt(0);

        /** 得到Excel的行数 */
        totalRows = sheet.getPhysicalNumberOfRows();
        /** 得到Excel的列数 */
        if (totalRows >= 1 && sheet.getRow(0) != null) {
            totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        //循环行
        for (int r = 0; r < totalRows; r++) {
            Row row = sheet.getRow(r);
            String cellValue = "";
            String cellKey = "";
            if (row == null) {
                continue;
            }
            /** 循环Excel的列 */
            for (int c = 0; c < totalCells; c++) {
                Cell cell = row.getCell(c);

                if (null == cell) {
                    continue;
                }
                // 以下是判断数据的类型
                switch (cell.getCellType()) {
                    case HSSFCell.CELL_TYPE_NUMERIC: // 数字
                        DecimalFormat df = new DecimalFormat("0");
                        cellKey = df.format(cell.getNumericCellValue());
                        break;
                    case HSSFCell.CELL_TYPE_STRING: // 字符串
                        cellValue = cell.getStringCellValue();
                        break;
                }
            }
            //System.out.println("===: "+cellKey);
            //System.out.println("+++: "+cellValue);
            map.put(cellKey,cellValue);
        }

    }
}
