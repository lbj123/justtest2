package testSolrExceldata;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class SolrRequestForL {
    public static final String solrForJD="https://solr:SolrRocks@zpsearch.zhaopin.com/solr-position-pre/positionCloud/select";//JDsolr地址
    public static final String excelTaget = "E:\\workspaceNoOne\\workspace\\test1.xlsx";//excel data路径

    public static final String clear = "E:\\workspaceNoOne\\excel_vba\\clear.csv";//存放数据文件1
    public static final String unclear = "E:\\workspaceNoOne\\excel_vba\\unclear.csv";//存放数据文件2

    static String paramOne = "fl=SOU_POSITION_ID,SOU_POSITION_NAME&q=JD_JOB_LEVEL:";//参数
    static String paramTwo = "%20AND%20(SOU_POSITION_TYPE:2)&rows=50";//参数

    public static void main(String[] args) throws Exception {
        String path = restult();
        System.out.println(path);
    }

    public static String restult() throws Exception{

        Map map = new HashMap();
        map = readExcel(excelTaget);
        String param = "";
        JSONObject resJson = null;
        File f=new File(clear);//明确
        FileWriter writer = new FileWriter(f , false);
        BufferedWriter out = new BufferedWriter(writer);

        File f2=new File(unclear);//不明确
        FileWriter writer2 = new FileWriter(f2 , false);
        BufferedWriter out2 = new BufferedWriter(writer2);

        //循环map，发送请求，获取结果
        for(Object key : map.keySet()){
            if("".equals(key) || null == key){
                continue;
            }
            param = paramOne + key.toString() + paramTwo;
            resJson = doGet(solrForJD, param);
            JSONObject response = resJson.getJSONObject("response");
            JSONArray listArray = response.getJSONArray("docs");  // 获取列表。即，json串中[]的内容
            //
            int listSize = listArray.size();
            for(int i=0;i<listSize;i++){
                JSONObject sou = (JSONObject)listArray.get(i);
                String souName = sou.getString("SOU_POSITION_NAME");//例如：四川 成都 就业有位来####2020届校园招聘岗位（活水管培生）
                String souId = sou.getString("SOU_POSITION_ID");//例如：CC120880480J00585071701
                if(souName.indexOf((String)map.get(key)) ==-1){ //不包含，则为不明确职位
                    //写到不明确csv文件中
                    out2.write(souId+"\r\n");
                }else{//包含，明确职位，写道明确csv文件中
                    out.write(souId+"\r\n");
                }
            }
        }
        out.flush();
        out.close();
        out2.flush();
        out2.close();
        return "clear:"+clear +"\nunclear:"+ unclear;
    }

    /**
    *@Author 田原
    *@function 连接solr，获取返回json
    */
    public static JSONObject doGet(String url, String params) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(url + "?" + params);
        // 响应模型
        CloseableHttpResponse response = null;
        try {
            // 配置信息
            RequestConfig requestConfig = RequestConfig.custom()
                    // 设置连接超时时间(单位毫秒)
                    .setConnectTimeout(5000)
                    // 设置请求超时时间(单位毫秒)
                    .setConnectionRequestTimeout(5000)
                    // socket读写超时时间(单位毫秒)
                    .setSocketTimeout(5000)
                    // 设置是否允许重定向(默认为true)
                    .setRedirectsEnabled(true).build();

            httpGet.setConfig(requestConfig);

            response = httpClient.execute(httpGet);
            HttpEntity responseEntity = response.getEntity();
            JSONObject responseJson = JSON.parseObject(EntityUtils.toString(responseEntity));
            return responseJson;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Map<String,String> readExcel(String path) throws Exception{
        File xlsFile = new File(path);
        Map<String,String> map = new HashMap<String,String>();
        // 工作表
        Workbook workbook = WorkbookFactory.create(xlsFile);
        /** 得到第一个sheet */
        Sheet sheet = workbook.getSheetAt(0);
        /** 得到Excel的行数 */
        int totalRows = sheet.getPhysicalNumberOfRows();
        /** 得到Excel的列数 */
        int totalCells = 0;
        if (totalRows >= 1 && sheet.getRow(0) != null) {
            totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        //循环行
        for (int r = 0; r < totalRows; r++) {
            Row row = sheet.getRow(r);
            String cellValue = "";
            String cellKey = "";
            if (row == null) {
                continue;
            }
            /** 循环Excel的列 */
            for (int c = 0; c < totalCells; c++) {
                Cell cell = row.getCell(c);

                if (null == cell) {
                    continue;
                }
                // 以下是判断数据的类型
                switch (cell.getCellType()) {
                    case HSSFCell.CELL_TYPE_NUMERIC: // 数字
                        DecimalFormat df = new DecimalFormat("0");
                        cellKey = df.format(cell.getNumericCellValue());
                        break;
                    case HSSFCell.CELL_TYPE_STRING: // 字符串
                        cellValue = cell.getStringCellValue();
                        break;
                }
            }
            //System.out.println("===: "+cellKey);
            //System.out.println("+++: "+cellValue);
            map.put(cellKey,cellValue);
        }
        return map;
    }
}
