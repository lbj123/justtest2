package testSolrExceldata;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SolrRequest3 {

    public static final String solr8requestUrl = "https://guest:guest@zpsearch.zhaopin.com/solr-resumeRecommend-pre/resumeRecommend/select";
    public static final String solr7requestUrl = "https://solr:SolrRocks@zpsearch.zhaopin.com/solr-resume-pre/resumeCloud/select";

//    public static final String param_q ="q=*:*&fq=ID:%s&fl=ID";
    public static final String param_q ="q=*:*";
    public static final String param_fl="&fl=ID";
    public static final String param_fq="fq=ID:%s";

    //TODO 考虑其他语句的判断条件
    public static String getSolrField(String expre){
        String field;
        int startIndex = expre.indexOf("qf='");
        if(startIndex == -1){
            startIndex = expre.indexOf("f='");
            startIndex += "f='".length();
            int fieldEndIndex = expre.indexOf("'",startIndex);
            field = expre.substring(startIndex,fieldEndIndex);
        }else{
            startIndex += "qf='".length();
            int fieldEndIndex = expre.indexOf("'",startIndex);
            field = expre.substring(startIndex,fieldEndIndex);
        }
        return field;

    }

    public static String getSolrRequestParam(String cvId,String[] fl,String[] fq) throws UnsupportedEncodingException {
        String fqSolr = String.format(param_fq,cvId);
        String flSolr = null;
        boolean containsChildJob = false;
        boolean containsPerferrence = false;
        if(fl != null && fl.length > 0){
            flSolr = param_fl + ",";
            for(int i = 0 ;i < fl.length;i++){
                flSolr += fl[i] ;
                if(fl[i].contains("CV_PREFERREDS.")){
                    containsPerferrence= true;
                }
                if(fl[i].contains("CV_JOBS.")){
                    containsChildJob=true;
                }
                if(i != fl.length - 1){
                    flSolr += ",";
                }
            }
            if(containsChildJob || containsPerferrence){
                flSolr += "," + "[child]";
                if(containsChildJob){
                    flSolr+="," + "CHILD_CV_JOBS";
                    flSolr+="," + "CV_JOBS.VALID_FLAG";
                }
                if(containsPerferrence){
                    flSolr+="," + "CHILD_CV_PREFERREDS";
                }
            }
        }
        if(fq != null){
            for(int i = 0 ;i < fq.length;i++){
                fqSolr += "&fq=" + URLEncoder.encode(fq[i],"UTF-8");
            }
        }
        String tmp =  param_q+  flSolr +"&" +  fqSolr;
        return tmp;
    }

//    public static void main(String[] args) throws UnsupportedEncodingException {
//        String cvID = "JM333338407R90500000000_1";
//        String param = String.format(findExist_param, cvID);
////        param = URLEncoder.encode(param,"UTF-8");
//        JSONObject resJson = doGet(solr8requestUrl, param);
//        JSONObject response = resJson.getJSONObject("response");
//        Integer num = response.getInteger("numFound");
//        System.out.println(num);
//        System.out.println(resJson.toJSONString());
//
//        resJson = doGet(solr7requestUrl, param);
//        response = resJson.getJSONObject("response");
//        num = response.getInteger("numFound");
//        System.out.println(num);
//        System.out.println(resJson.toJSONString());
//    }



    public static JSONObject doGet(String url, String params) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(url + "?" + params);
        // 响应模型
        CloseableHttpResponse response = null;
        try {
            // 配置信息
            RequestConfig requestConfig = RequestConfig.custom()
                    // 设置连接超时时间(单位毫秒)
                    .setConnectTimeout(5000)
                    // 设置请求超时时间(单位毫秒)
                    .setConnectionRequestTimeout(5000)
                    // socket读写超时时间(单位毫秒)
                    .setSocketTimeout(5000)
                    // 设置是否允许重定向(默认为true)
                    .setRedirectsEnabled(true).build();

            httpGet.setConfig(requestConfig);

            response = httpClient.execute(httpGet);
            HttpEntity responseEntity = response.getEntity();
            JSONObject responseJson = JSON.parseObject(EntityUtils.toString(responseEntity));
            return responseJson;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
