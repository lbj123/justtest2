package tezhengchayi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.sound.midi.SoundbankResource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Compare4 {
    //exp
    public static String bigTree_exp = "config-resume_recommend-resume_talents-default-v1.0-publicStore-Feature";

    //下边三行结果，需要改一下  baseline
    public static String publicStore_exp = "config-resume_recommend-resume_talents-default-v1.0-publicStore-optimize";
    public static String rulePath = "/ROOT/Translatetranslaterv10";

    public static String jobNumbers ="CC478275720J40113829203 CCL1251918390J40119763314 CC626522820J40116803910 CCL1319463360J40121657307 CC667744820J40109897716 CCL1231831630J40080425407 CC301172130J40038234104 CCL1246361740J00235367614 CC294768182J00141264609 CC321588483J90250076000";
    public static String compareResultFile = "e:\\compare.json";
    public static void main(String[] args) throws IOException {


        String url = "https://zpsearch.zhaopin.com/aggregatorpre/resume-recommend/resume/talents";
        String onlineUrl = "https://zpsearch.zhaopin.com/aggregator/resume-recommend/resume/talents";
        String[] jns = jobNumbers.split(" ");

        JSONObject resultJson = new JSONObject();
        for(int i = 0 ;i < jns.length;i++){
            JSONObject jsonObj = new JSONObject();
            JSONObject solr = new JSONObject();
            JSONArray fq = new JSONArray();
            jsonObj.put("solrExp" ,solr);
            jsonObj.put("fq",fq);
            String jobNumber = jns[i];
            resultJson.put(jobNumber,jsonObj);
            String publicStoreParam = getParam(publicStore_exp, jobNumber,null);
            String bigTreeParam = getParam(bigTree_exp, jobNumber,rulePath);



            JSONObject publicStoreResult = null;
            JSONObject publicStoreRules = null;
            boolean retryFlag = true;
            while(retryFlag) {
                try {
                    publicStoreResult = httpPost(url, publicStoreParam);
                    publicStoreRules = getRuneRank(publicStoreResult);
                    retryFlag = false;
                } catch (Exception e) {
                    System.out.println("开始重试publicStore" + jobNumber);
                }
            }
            JSONObject bigTreeResult = null;
            JSONObject bigTreeRules = null;
            retryFlag = true;
            while(retryFlag) {
                try {

                    bigTreeResult = httpPost(url, bigTreeParam);
                    bigTreeRules = getRuneRank(bigTreeResult);
                    retryFlag = false;

                } catch (Exception e) {
                    System.out.println("开始重试BigTree" + jobNumber);
                }
            }


            System.out.println(jobNumber + "   rule size same:" + (publicStoreRules.size() == bigTreeRules.size()));
            for(Map.Entry<String,Object> entry:publicStoreRules.entrySet()){
                String publicStoreSolr = "";
                String bigTreeSolr = "";
                String ruleName = entry.getKey();
                JSONObject valueJson = (JSONObject) entry.getValue();
                JSONObject contextJson = valueJson.getJSONObject("context");
                if(contextJson != null){
                    publicStoreSolr = contextJson.getString("context");
                }

                valueJson = null;
                for(Map.Entry<String,Object> ee : bigTreeRules.entrySet()){
                    String rn = ee.getKey();
                    if(rn.contains(ruleName)){
                        valueJson = (JSONObject)ee.getValue();
                    }
                }
//                valueJson = bigTreeRules.getJSONObject(ruleName+"_fex");



                contextJson = valueJson.getJSONObject("context");
                if(contextJson != null){
                    bigTreeSolr = contextJson.getString("context");
                }
                if(!bigTreeSolr.equals(publicStoreSolr)){
                    JSONObject ruleJsonObj = new JSONObject();
                    ruleJsonObj.put("publicS",publicStoreSolr);
                    ruleJsonObj.put("bigTree",bigTreeSolr);
                    solr.put(ruleName,ruleJsonObj);
                }

            }
            int k =0;
            //fq
            JSONArray pubFq = publicStoreResult.getJSONObject("debugs").getJSONObject("invokeInfo").getJSONArray("solr").getJSONObject(0).getJSONObject("request").getJSONObject("param").getJSONArray("fq");
            JSONArray bigtreeFq =   bigTreeResult.getJSONObject("debugs").getJSONObject("invokeInfo").getJSONArray("solr").getJSONObject(0).getJSONObject("request").getJSONObject("param").getJSONArray("fq");
            for(int index = 0 ;index < pubFq.size();index++){
                /*System.out.println("k = " + k);
                k=k+1;
                if(k==7){
                    System.out.println("jinru tiaoshi = ");
                }*/
                String pubFqExp = pubFq.getString(index);
                String bigtreeFqExp = bigtreeFq.getString(index);
                if(!pubFqExp.equals(bigtreeFqExp)){
                    JSONObject fqcompare = new JSONObject();
                    fqcompare.put("bigTree",bigtreeFqExp);
                    fqcompare.put("publicS",pubFqExp);
                    fq.add(fqcompare);
                }
            }
        }
        FileOutputStream fos = new FileOutputStream(new File(compareResultFile));
        fos.write(JSON.toJSONString(resultJson, SerializerFeature.DisableCircularReferenceDetect).getBytes(Charset.forName("UTF-8")));
        fos.close();

    }

    private static JSONObject getRuneRank(JSONObject resultJson){
        JSONObject debugsJson = resultJson.getJSONObject("debugs");
        JSONObject ruleRankNode = debugsJson.getJSONObject("ruleRank");
        JSONObject rr0Json = ruleRankNode.getJSONObject("0");
        JSONObject ruleRank = rr0Json.getJSONObject("rankRule");
        return ruleRank;
    }




    private static String getParam(String exp, String jobNumber,String rulePath) {
        JSONObject jo = new JSONObject();
        //debug
        JSONObject debug = new JSONObject();
        debug.put("simple", true);
        debug.put("exp", exp);
//        debug.put("function", true);
        jo.put("debug", debug);
        //debugs
        JSONObject debugs = new JSONObject();
        JSONObject system = new JSONObject();
        system.put("rule", true);
        if(rulePath != null){
//            system.put("rulePath",rulePath);
        }
        debugs.put("system", system);
        jo.put("debugs", debugs);
        //============
        jo.put("aggregator_protocol", "rpc");
        jo.put("CompanyId", "39493793");
        jo.put("aggregator_core_version_resume_search_resume_v3", "1.1");
        jo.put("start", "0");
        jo.put("rows", "0");
        jo.put("S_DISCLOSURE_LEVEL", "3:2");
        jo.put("eventScenario", "talents");
        jo.put("UserId", "1049895014");
        jo.put("client", "homepage");
        jo.put("DepartmentId", "39493793");
        jo.put("jobNumber", jobNumber);
        jo.put("rankStype_1", true);

        JSONObject phase = new JSONObject();
        JSONObject experiment = new JSONObject();
        JSONObject custom = new JSONObject();
        custom.put("timeout",5000000);

        experiment.put("custom",custom);
        phase.put("experiment", experiment);
        debugs.put("phase", phase);
        return jo.toJSONString();
    }
    private static JSONObject httpPost(String url, String param) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(url);

        StringEntity entity = new StringEntity(param, "UTF-8");

        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type", "application/json;charset=utf8");
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();

            if (responseEntity != null) {
                String content = EntityUtils.toString(responseEntity);
                return JSONObject.parseObject(content);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
