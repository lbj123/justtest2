package jmeter.test.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SendCan extends AbstractFunction {
    private static final List<String> desc = new LinkedList<String>();  /*该变量用来获取对参数值输入进行描述*/

    private static final String KEY = "SendCan";  /*这个是在函数助手上显示的函数名称*/

    private Object[] values;

    public String connect = "&";

    public SendCan(){

    }

    static {
        desc.add("请输入第一个参数，类似variable=value,其中变量值可以写成变量格式，格式为${value}，如果有多个参数，只需要用&连接"
                + "类似variable=value&variable1=value1,第一个参数也可以置空，然后在Body data中填写也行");
        desc.add("请输入第二个参数，该参数为变化的序列id，需要和jmeter中获取的变量名保持一致");
        desc.add("请输入第三个参数，该参数为申明重复参递增参数名是否一样，填否则序列递增，如id[0]、id[1]，不填则默认相同");
        desc.add("请输入第四个参数，该参数值是用来在jmeter的Body_Data用来调用的，可以根据需要自定义,不填则默认为第二个参数名");
        desc.add("请输入第五个参数，该参数值是用来在jmeter的Body_Data用来调用的，可以根据需要自定义,不填则默认为第二个参数名");
        /*该描述是针对函数的输入值进行描述*/
    }

    @Override
    public String execute(SampleResult sampleResult, Sampler sampler) throws InvalidVariableException {
        String cvActiveTimeLast = ((CompoundVariable)values[0]).execute();//第一个输入框的值
        String cvPreSal = ((CompoundVariable)values[1]).execute();//第二个输入框的值
        String cvEduLevel = ((CompoundVariable)values[2]).execute();//第三个输入框的值
        String resumeBackground = ((CompoundVariable)values[3]).execute();//第四个输入框的值
        String workYears = ((CompoundVariable)values[4]).execute();//第五个输入框的值
        String matchNr = cvPreSal+"_matchNr";
        JMeterVariables jMeterVariables = getVariables();
        matchNr = jMeterVariables.get(matchNr);//获取jmeter中变量匹配的数量
        int matchNr_int  = 0;
        matchNr_int = Integer.valueOf(matchNr);
        String save = null;
        if(cvEduLevel.length()>0){
            for (int i=0;i<=matchNr_int;i++){
                int sum = i-1;
                String temp = cvPreSal+"["+sum + "]"+"="+jMeterVariables.get(cvPreSal+"_"+i);
                if (i==1){
                    save = temp;
                }else{
                    save = save+connect+temp;
                }
            }
        }else{
            for(int i=0;i<=matchNr_int;i++){
                String temp = cvPreSal+"="+jMeterVariables.get(cvPreSal+"_"+i);
                if (i==1){
                    save = temp;
                }else{
                    save = save+connect+temp;
                }
            }
        }
        //该判断用来看参数是否包含其他字段请求
        if(cvActiveTimeLast.length()>0){
            save = cvPreSal+connect+save;
        }
        //用来判断是把变量保存到自定义的变量，还是保存在第二个获取变量的参数中
        if(resumeBackground.length()>0){
            jMeterVariables.put(resumeBackground,save);
        }else{
            jMeterVariables.put(cvPreSal,save);
        }

        return null;
    }
    @Override
    public void setParameters(Collection<CompoundVariable> parameters) throws InvalidVariableException {
        /*该函数用来获取输入的值*/
        /*这里，后面的1，2分别指接受的最小参数个数为1，以及接受的最大参数个数为5*/
        checkParameterCount(parameters,1,5);
        values = parameters.toArray();
        /*将值存入类变量中，把接受到的参数装成array，然后用value[i]取出来 */
    }
    @Override
    public String getReferenceKey() {
        /*本方法是提供一个在Jmeter函数助手显示的函数名称*/
        return KEY;
    }
    @Override
    public List<String> getArgumentDesc() {
        /*该函数用来获取对输入参数的描述*/
        return desc;
    }
}
