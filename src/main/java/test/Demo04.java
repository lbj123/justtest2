package test;

public enum Demo04 {
    S_DESIRED_SALARY(false),
    S_WORK_YEARS(false),
    S_EDUCATION(false),
    S_EXCLUSIVE_RESUME_BACKGROUND(false),
    S_CV_ACTIVE_TIME_LAST_TEST(false);

    private boolean jude;

    Demo04(boolean judg){
        this.jude = judg;
    }

    public boolean isJude() {
        return jude;
    }

    public void setJude(boolean jude) {
        this.jude = jude;
    }
}
