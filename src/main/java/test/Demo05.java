package test;

public class Demo05 {
    boolean desrSalary = false;
    boolean workYears = false;
    boolean edu = false;
    boolean background = false;
    boolean activeTime = false;

    public boolean isDesrSalary() {
        return desrSalary;
    }

    public void setDesrSalary(boolean desrSalary) {
        this.desrSalary = desrSalary;
    }

    public boolean isWorkYears() {
        return workYears;
    }

    public void setWorkYears(boolean workYears) {
        this.workYears = workYears;
    }

    public boolean isEdu() {
        return edu;
    }

    public void setEdu(boolean edu) {
        this.edu = edu;
    }

    public boolean isBackground() {
        return background;
    }

    public void setBackground(boolean background) {
        this.background = background;
    }

    public boolean isActiveTime() {
        return activeTime;
    }

    public void setActiveTime(boolean activeTime) {
        this.activeTime = activeTime;
    }
}
