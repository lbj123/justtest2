package test;

public final class Demo03 {
    static final String salary = "+(({!edismax  qf='CV_PREFERRED_SALARY1' v='0200104000' q.op=or} OR {!edismax  qf='CV_PREFERRED_SALARY2' v='0200104000' q.op=or} OR {!edismax  qf='CV_PREFERRED_SALARY3' v='0200104000' q.op=or} OR {!qrange f='CV_VALID_SALARY' l='2001'  u='4000'  v=''} OR {!edismax  qf='CV_VALID_SALARY' v='0000000000' q.op=or}))";
    static final String workYear = "+({!qrange f='WORK_YEARS' l='201412'  u='202012'  v=''})";
    static final String edu = "+({!edismax  qf='CV_EDU_LEVEL' v='4 5' q.op=or})";
    static final String background = " -({!edismax  qf='RESUME_BACKGROUND' v='8' q.op=or})";
    static final String activeTime = "+({!qrange f='CV_ACTIVE_TIME_LAST' l='2020-01-06T07:00:00Z'  u='2020-12-09T07:00:00Z'  v=''})";

    static final String preSalary="CV_PREFERRED_SALARY";
    static final String workYears="WORK_YEARS";
    static final String eduLevel="CV_EDU_LEVEL";
    static final String resBackground="RESUME_BACKGROUND";
    static final String actTime="CV_ACTIVE_TIME_LAST";

}
