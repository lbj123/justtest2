package test;

public enum Demo02 {
    SALARY("+(({!edismax  qf='CV_PREFERRED_SALARY1' v='0200104000' q.op=or} OR {!edismax  qf='CV_PREFERRED_SALARY2' v='0200104000' q.op=or} OR {!edismax  qf='CV_PREFERRED_SALARY3' v='0200104000' q.op=or} OR {!qrange f='CV_VALID_SALARY' l='2001'  u='4000'  v=''}))"),
    WORK_YEAR("+({!qrange f='WORK_YEARS' l='201508'  u='201707'  v=''})"),
    EDU("+({!edismax  qf='CV_EDU_LEVEL' v='4 5' q.op=or})"),
    BACKGROUND("-({!edismax  qf='RESUME_BACKGROUND' v='8' q.op=or})"),
    ACTIVE_TIME("+({!qrange f='CV_ACTIVE_TIME_LAST' l='2020-10-06T07:00:00Z'  u='2020-11-13T07:00:00Z'  v=''})");

    private String typeName;
    Demo02(String typeName){
        this.typeName = typeName;
    }
    public String getName() {
        return typeName;
    }
}
