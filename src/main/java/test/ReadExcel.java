package test;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.util.*;


public class ReadExcel {

    public static void main(String[] args) throws Exception {
        Map a = readExcel(3,7,8,4,0,2);
        Set<String> keySet = a.keySet();
        Iterator<String> it1 = keySet.iterator();
        //while(it1.hasNext()){
          //  String key = it1.next();
           // System.out.println(key+"，"+((List)(a.get(key))).get(0)+"，" + ((List)(a.get(key))).get(1) +"，" +((List)(a.get(key))).get(2) +"，" +((List)(a.get(key))).get(3) + "，" +((List)(a.get(key))).get(4));
        //}
        //http://127.0.0.1:8000/common_rule_test/insert_data_to_rule_table_view?rule_id=1&rule_cv_field_desc=1&rule_jd_filed_desc=1&rule_group_name=1&rule_group_desc=1&rule_desc=1

        System.out.println();
    }

    static String keyWordExcel = "E:\\my\\123.xlsx";
    static String keyWordSheet = "Sheet1";

    /** 总行数 */
    private static int totalRows = 0;

    /*
    * 如果thirdName == ”“，list为全部值
    * 如果thirdName == ”一个不存在的值“，list为空
    * 如果thirdName == ”存在的值“，list为符合的值
    * */
    // map("CvProfIdx00006MatGrpOneJdSkillFeat00005",list)
    //list("MatGrpOne","CV专业技能之一 = JD职位技能要求标签")
    public static Map<String, List> readExcel(int match, int thirdNameCell, int keyWordCell,int jddesc,int cvdesc,int matchdesc) throws Exception {
        File xlsFile = new File(keyWordExcel);
        Map<String,List> map = new HashMap<String,List>();


        // 工作表
        Workbook workbook = WorkbookFactory.create(xlsFile);
        /** 得到第一个shell */
        Sheet sheet = workbook.getSheet(keyWordSheet);
        /** 得到Excel的行数 */
        totalRows = sheet.getPhysicalNumberOfRows();

        //1.循环totalRows行数，获取 thirdNameCell 列的数据
        for(int i = 0; i < totalRows; i++){
            Row row = sheet.getRow(i);//i行
            Cell rowForTNC = row.getCell(thirdNameCell);
            String rowForTN = "";
            if(rowForTNC.getCellType() == Cell.CELL_TYPE_STRING){
                rowForTN = rowForTNC.getStringCellValue();
            }else if(rowForTNC.getCellType() == Cell.CELL_TYPE_NUMERIC){
                rowForTN = String.valueOf(rowForTNC.getNumericCellValue());
            }
            if(null == rowForTN || "".equals(rowForTN)){
                continue;
            }
            //获取函数名称 例如： CvJobIdx00023MatOneJdSkillFeat00003
            String b = rowForTN;//CvJobIdx00005MatGrpOneJdTitleFeat00014
            //例如：CV专业技能之一 = JD职位技能要求标签
            Cell rowForKWC = row.getCell(keyWordCell);
            String expl = rowForKWC.getStringCellValue();

            //例如 ： MatOne
            Cell rowForMatch = row.getCell(match);
            String matchStr = rowForMatch.getStringCellValue();

            //jd描述
            Cell jddescCell = row.getCell(jddesc);
            String jddescStr = jddescCell.getStringCellValue();

            //cv描述
            Cell cvdescCell = row.getCell(cvdesc);
            String cvdescStr = cvdescCell.getStringCellValue();

            //matchdesc  例如： 至少包含一组分词
            Cell matchdescCell = row.getCell(matchdesc);
            String matchdescStr = matchdescCell.getStringCellValue();

            //        //http://127.0.0.1:8000/common_rule_test/insert_data_to_rule_table_view?rule_id=1&rule_cv_field_desc=1&
            //        rule_jd_filed_desc=1&rule_group_name=1&rule_group_desc=1&rule_desc=1

            String param = "rule_id="+rowForTN+"&rule_cv_field_desc="+cvdescStr+"&rule_jd_filed_desc="+jddescStr+"&rule_group_name="+matchStr+"&rule_group_desc="+matchdescStr+"&rule_desc="+expl;
            String params[] = new String[6];
            params[0] = rowForTN;
            params[1] = cvdescStr;
            params[2] = jddescStr;
            params[3] = matchStr;
            params[4] = matchdescStr;
            params[5] = expl;




//{
//    "rule_id": "CvJobIdx00010MatOneJdCateFeat00005",
//"rule_cv_field_desc": "1",
//    "rule_jd_filed_desc": "1",
//    "rule_group_name": "1",
//    "rule_group_desc": "1",
//    "rule_desc": "1"
//}
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("rule_id",rowForTN);
            jsonObject.put("rule_cv_field_desc",cvdescStr);
            jsonObject.put("rule_jd_filed_desc",jddescStr);
            jsonObject.put("rule_group_name",matchStr);
            jsonObject.put("rule_group_desc",matchdescStr);
            jsonObject.put("rule_desc",expl);

            SolrRequestForL.sendPost("http://10.2.6.148:8000/common_rule_test/insert_data_to_rule_table_view",jsonObject);

            List<String> list = new ArrayList<String>();
            list.add(matchStr);
            list.add(expl);
            list.add(jddescStr);
            list.add(cvdescStr);
            list.add(matchdescStr);

            map.put(rowForTN,list);





        }
        return map;
    }
}