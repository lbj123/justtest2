package testForEmptyResult;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author 李蓓杰
 * @function 处理空结果集，获取map,key为job_num，value为baseline_count
*/
public class ProduceData {
    static String emptyResult = Path.emptyResult;

    public static void main(String[] args) throws IOException{
        String aaa = filePro();
        Map ad = produceData(aaa);

    }

    //JSONObject responseObj = JSON.parseObject(emptyResult);  // 整个Response作为JSON对象
    //String repCode = responseObj.getString("code");

    public static String filePro() throws IOException {
        String jsonStr = "";
        //读取结果，封装成json
        File jsonFile = new File(emptyResult);
        FileReader fileReader = new FileReader(jsonFile);
        Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
        int ch = 0;
        StringBuffer sb = new StringBuffer();
        while ((ch = reader.read()) != -1) {
            sb.append((char) ch);
        }
        fileReader.close();
        reader.close();
        jsonStr = sb.toString();
        return jsonStr;
    }

    public static Map produceData(String jsonStr){
        Map<String,Integer> map = new HashMap<String,Integer>();
        String jobNum = "";
        String baselineCount = "";
        String exp_url = "";
        JSONObject responseObj = JSON.parseObject(jsonStr);
        String repCode  = responseObj.getString("code");
        if(!("200".equals(repCode))){
            return null;
        }
        JSONArray listArray = responseObj.getJSONArray("data");
        int len = listArray.size();
        for(int i= 0;i<len;i++){
            if("solrEmptyResultError".equals(((JSONObject)listArray.get(i)).getString("exp_exception"))){
                jobNum = ((JSONObject)listArray.get(i)).getString("job_num");
                baselineCount = ((JSONObject)listArray.get(i)).getString("baseline_count");
                exp_url = ((JSONObject)listArray.get(i)).getString("exp_url");
                map.put(jobNum,Integer.valueOf(baselineCount));
                //map.put(exp_url,Integer.valueOf(baselineCount));
            }
        }
        //排序，把差距最大的放在前面
        Map<String,Integer> finalOut = new LinkedHashMap<>();
        map.entrySet()
                .stream()
                .sorted((p1, p2) -> p2.getValue().compareTo(p1.getValue()))
                .collect(Collectors.toList()).forEach(ele -> finalOut.put(ele.getKey(), ele.getValue()));

        for(String key:finalOut.keySet()){
            System.out.println(key+":"+finalOut.get(key));
        }
        return finalOut;
    }
}
